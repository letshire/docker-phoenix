FROM elixir:1.7-alpine

ARG PHOENIX_ARCHIVE_NAME

# Install Phoenix dependencies
RUN apk add --no-cache inotify-tools g++

# Install Phoenix
# https://hexdocs.pm/phoenix/installation.html
RUN mix local.hex --force \
    && mix local.rebar --force \
    && mix archive.install https://github.com/phoenixframework/archives/raw/master/$PHOENIX_ARCHIVE_NAME --force \
    && mkdir -p /usr/app/phoenix_app

WORKDIR /usr/app/phoenix_app

# Initiate a Phoenix app
RUN echo y | mix phx.new --no-brunch . \
    && rm config/prod*

# configure to use sqlite
COPY phoenix_app/mix.exs mix.exs
COPY phoenix_app/config/* config/
COPY phoenix_app/bin bin

EXPOSE 4000

CMD bin/start.sh
