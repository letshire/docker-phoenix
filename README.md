Phoenix Docker Image
==========================

This repository intends to build a Phoenix base image for *development*. It is not
optimized for production use.

In particular, this Phoenix image is meant to be used for API server and therefore
`node` and asset pipeline is not supported.


# Running a container with the default image

A default Phoenix app has been installed for testing. It can be run using:

```sh
$ docker run --rm -p4000:4000 phoenix-api:1.3.0
```

## Configuring the app

TODO

# Building a Phoenix image

```sh
$ ops/build.sh <tagging_version>

# e.g. building phoenix-api:1.3.0
$ ops/build.sh 1.3.0
```

# Deploy

Gitlab CI build, test, and deploy images for us.

It tags the built images using the **branch name** or **tag name**.

## Rebuilding image to update only the dependencies

Sometimes Phoenix itself although hasn't been updated, dependencies such as `mix`
could have been updated. In this case, when the dependencies should be updated,
please trigger a rebuild on Gitlab CI. **Never force push with a changed hash**.
