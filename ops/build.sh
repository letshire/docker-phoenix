#! /bin/bash

test -z $1 && echo Usage: 'ops/build.sh <tagging_version>' && exit 1

IMAGE_NAME_LOCAL=phoenix-api

[[ $1 = master ]] && IMAGE_TAG=latest || IMAGE_TAG=$(echo $1 | sed -e 's/^v//')
[[ $1 = master ]] && PHOENIX_ARCHIVE_NAME="phx_new.ez" || PHOENIX_ARCHIVE_NAME="phx_new-$IMAGE_TAG.ez"

echo "Building $IMAGE_NAME_LOCAL:$IMAGE_TAG..."

docker build --build-arg PHOENIX_ARCHIVE_NAME=$PHOENIX_ARCHIVE_NAME -t $IMAGE_NAME_LOCAL:$IMAGE_TAG .
